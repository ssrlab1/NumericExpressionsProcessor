<?php
	header("Content-type: text/html;  charset=utf-8");
	header("Access-Control-Allow-Origin: *");
	mb_internal_encoding('UTF-8');
	mb_regex_encoding('UTF-8');
	
	$text = isset($_POST['text']) ? $_POST['text'] : '';
	$checkbox1 = isset($_POST['checkbox1']) ? $_POST['checkbox1'] : 0;
	$checkbox2 = isset($_POST['checkbox2']) ? $_POST['checkbox2'] : 0;
	$checkbox3 = isset($_POST['checkbox3']) ? $_POST['checkbox3'] : 0;
	$checkbox4 = isset($_POST['checkbox4']) ? $_POST['checkbox4'] : 0;
	$checkboxes = array('checkbox1' => $checkbox1, 'checkbox2' => $checkbox2, 'checkbox3' => $checkbox3, 'checkbox4' => $checkbox4);
	
	include_once 'NumericExpressionsProcessor.php';
	
	$msg = '';
	if(!empty($text)) {
		$NumericExpressionsProcessor = new NumericExpressionsProcessor($checkboxes);
		$NumericExpressionsProcessor->setText($text);
		$NumericExpressionsProcessor->run();
		$NumericExpressionsProcessor->saveCacheFiles();
		
		$result['text'] = $text;
		$result['result'] = $NumericExpressionsProcessor->getResult();
		$msg = json_encode($result);
	}
	else {
		$result['text'] = $text;
		$result['result'] = 'Вы даслалі пусты запыт!';
		$msg = json_encode($result);
	}
	echo $msg;
?>