<?php
	mb_internal_encoding("UTF-8");
	
	class NumericExpressionsProcessor {
		private static $localizationArr = array();
		private static $localizationErr = array();
		private $text = "";
		private $selectedCheckboxes = array();
		private $result = "";
		const BR = "<br>\n";
		
		function __construct($selectedCheckboxes) {
			$this->selectedCheckboxes = $selectedCheckboxes;
		}
		
		function __destruct() {
			
		}
		
		public function setText($text) {
			$this->text = $text;
		}
		
		public static function loadLanguages() {
			$languages = array();
			$files = scandir('lang/');
			if(!empty($files)) {
				foreach($files as $file) {
					if(substr($file, 2, 4) == '.txt') {
						$languages[] = substr($file, 0, 2);
					}
				}
			}
			return $languages;
		}
		
		public static function loadLocalization($lang) {
			$filepath = "lang/$lang.txt";
			$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			if($linesArr === false) {
				$filepath = "lang/en.txt";
				$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			}
			foreach($linesArr as $line) {
				if(empty($line)) {
					$key = $value = '';
				}
				elseif(substr($line, 0, 1) !== '#' && substr($line, 0, 2) !== '//') {
					if(empty($key)) {
						$key = $line;
					}
					else {
						if(!isset(self::$localizationArr[$key])) {
							self::$localizationArr[$key] = $line;
						}
					}
				}
			}
		}
		
		public static function showMessage($msg) {
			if(isset(self::$localizationArr[$msg])) {
				return self::$localizationArr[$msg];
			}
			else {
				self::$localizationErr[] = $msg;
				return $msg;
			}
		}
		
		public static function sendErrorList($lang) {
			if(!empty(self::$localizationErr)) {
				$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
				$sendersName = 'Numeric Expressions Processor';
				$recipient = 'corpus.by@gmail.com';
				$subject = "ERROR: Incorrect localization in $sendersName by user $ip";
				$mailBody = 'Вітаю, гэта corpus.by!' . self::BR;
				$mailBody .= $_SERVER['HTTP_HOST'] . '/NumericExpressionsProcessor/ дасылае інфармацыю аб наступных памылках:' . self::BR . self::BR;
				$mailBody .= "Мова лакалізацыі: <b>$lang</b>" . self::BR;
				$mailBody .= 'Адсутнічае лакалізацыя наступных ідэнтыфікатараў:' . self::BR . implode(self::BR, self::$localizationErr) . self::BR;
				$header  = "MIME-Version: 1.0\r\n";
				$header .= "Content-type: text/html; charset=utf-8\r\n";
				$header .= "From: $sendersName <corpus.by@gmail.com>\r\n";
				mail($recipient, $subject, $mailBody, $header);
			}
		}
		
		public function number2string($number, $unit = "") {
			static $dic = array(
				array(
					-2 => "дзве",
					-1 => "адна",
					1 => "адзін",
					2 => "два",
					3 => "тры",
					4 => "чатыры",
					5 => "пяць",
					6 => "шэсць",
					7 => "сем",
					8 => "восем",
					9 => "дзевяць",
					10 => "дзесяць",
					11 => "адзінаццаць",
					12 => "дванаццаць",
					13 => "трынаццаць",
					14 => "чатырнаццаць",
					15 => "пятнаццаць",
					16 => "шаснаццаць",
					17 => "сямнаццаць",
					18 => "васямнаццаць",
					19 => "дзевятнаццаць",
					20 => "дваццаць",
					30 => "трыццаць",
					40 => "сорак",
					50 => "пяцьдзясят",
					60 => "шэсцьдзясят",
					70 => "семдзясят",
					80 => "восемдзясят",
					90 => "дзевяноста",
					100 => "сто",
					200 => "дзвесце",
					300 => "трыста",
					400 => "чатырыста",
					500 => "пяцьсот",
					600 => "шэсцьсот",
					700 => "семсот",
					800 => "восемсот",
					900 => "дзевяцьсот"
				),
				array(
					array("", "", ""),
					array("тысяча", "тысячы", "тысяч"),
					array("мільён", "мільёны", "мільёнаў"),
					array("мільярд", "мільярды", "мільярдаў"),
					array("трыльён", "трыльёны", "трыльёнаў"),
					array("квадрыльён", "квадрыльёны", "квадрыльёнаў")
				),
				array(
					2, 0, 1, 1, 1, 2
				),
				array(
					"" => array("", "", ""),
					"dollar" => array("долар", "долары", "долараў"),
					"percent" => array("працэнт", "працэнты", "працэнтаў")
				)
			);

			$minus = "";
			if(mb_substr($number, 0, 1) == "-") {
				$minus = "мінус ";
				$number = mb_substr($number, 1);
			}
			
			$string = array();
			$number = str_pad($number, ceil(strlen($number) / 3) * 3, 0, STR_PAD_LEFT);
			$parts = array_reverse(str_split($number, 3));
			foreach($parts as $i => $part) {
				if($part > 0) {
					$digits = array();
					if($part > 99) {
						$digits[] = floor($part / 100)*100;
					}
					if($mod1 = $part % 100) {
						$mod2 = $part % 10;
						$flag = $i == 1 && $mod1 != 11 && $mod1 != 12 && $mod2 < 3 ? -1 : 1;
						if($mod1 < 20 || !$mod2) {
							$digits[] = $flag * $mod1;
						} else {
							$digits[] = floor($mod1 / 10) * 10;
							$digits[] = $flag * $mod2;
						}
					}
					$last = abs(end($digits));
					foreach($digits as $j => $digit) {
						$digits[$j] = $dic[0][$digit];
					}
					$case = (($last %= 100) > 4 && $last < 20) ? 2 : $dic[2][min($last % 10, 5)];
					if($i == 0) {
						$digits[] = $dic[3][$unit][$case];
					}
					else {
						$digits[] = $dic[1][$i][$case];
					}
					array_unshift($string, join(" ", $digits));
				}
			}
			return $minus . join(" ", $string);
		}
		
		public function number2case($number, $ending) {
			static $dic = array(
				1 => array("га" => "першага", "я" => "першая", "й" => "першай"),
				2 => array("га" => "другога", "я" => "другая", "й" => "другой"),
				3 => array("га" => "трэцяга", "я" => "трэцяя", "й" => "трэцяй"),
				4 => array("га" => "чацвёртага", "я" => "чацвёртая", "й" => "чацвёртай"),
				5 => array("га" => "пятага", "я" => "пятая", "й" => "пятай"),
				6 => array("га" => "шостага", "я" => "шостая", "й" => "шостай"),
				7 => array("га" => "сёмага", "я" => "сёмая", "й" => "сёмай"),
				8 => array("га" => "восьмага", "я" => "восьмая", "й" => "восьмая"),
				9 => array("га" => "дзявятага", "я" => "дзявятая", "й" => "дзявятай"),
				10 => array("га" => "дзясятага", "я" => "дзясятая", "й" => "дзясятай"),
				11 => array("га" => "адзінаццатага", "я" => "адзінаццатая", "й" => "адзінаццатай"),
				12 => array("га" => "дванаццатага", "я" => "дванаццатая", "й" => "дванаццатай"),
				13 => array("га" => "трынаццатага", "я" => "трынаццатая", "й" => "трынаццатай"),
				14 => array("га" => "чатырнаццатага", "я" => "чатырнаццатая", "й" => "чатырнаццатай"),
				15 => array("га" => "пятнаццатага", "я" => "пятнаццатая", "й" => "пятнаццатай"),
				16 => array("га" => "шаснаццатага", "я" => "шаснаццатая", "й" => "шаснаццатай"),
				17 => array("га" => "сямнаццатага", "я" => "сямнаццатая", "й" => "сямнаццатай"),
				18 => array("га" => "васямнаццатага", "я" => "васямнаццатая", "й" => "васямнаццатай"),
				19 => array("га" => "дзевятнаццатага", "я" => "дзевятнаццатая", "й" => "дзевятнаццатай"),
				20 => array("га" => "дваццатага", "я" => "дваццатая", "й" => "дваццатай"),
				21 => array("га" => "дваццаць першага", "я" => "дваццаць першая", "й" => "дваццаць першай"),
				22 => array("га" => "дваццаць другога", "я" => "дваццаць другая", "й" => "дваццаць другой"),
				23 => array("га" => "дваццаць трэцяга", "я" => "дваццаць трэцяя", "й" => "дваццаць трэцяй"),
				24 => array("га" => "дваццаць чацвёртага", "я" => "дваццаць чацвёртая", "й" => "дваццаць чацвёртай"),
				25 => array("га" => "дваццаць пятага", "я" => "дваццаць пятая", "й" => "дваццаць пятай"),
				26 => array("га" => "дваццаць шостага", "я" => "дваццаць шостая", "й" => "дваццаць шостай"),
				27 => array("га" => "дваццаць сёмага", "я" => "дваццаць сёмая", "й" => "дваццаць сёмай"),
				28 => array("га" => "дваццаць восьмага", "я" => "дваццаць восьмая", "й" => "дваццаць восьмая"),
				29 => array("га" => "дваццаць дзявятага", "я" => "дваццаць дзявятая", "й" => "дваццаць дзявятай"),
				30 => array("га" => "трыццатага", "я" => "трыццатая", "й" => "трыццатай"),
				31 => array("га" => "трыццаць першага", "я" => "трыццаць першая", "й" => "трыццаць першай"),
			);
			if(isset($dic[$number][$ending])) {
				return $dic[$number][$ending];
			}
			return false;
		}
		
		public function date2text($day, $month, $year) {
			$resultArr = array();
			$resultArr[] = $this->number2case($day, "га");
			$resultArr[] = $month;
			$year = intval($year) - 2000;
			if($year == 0) {
				$resultArr[] = "двухтысячнага";
			}
			else {
				$resultArr[] = "дзве тысячы " . $this->number2case($year, "га");
			}
			return implode(" ", $resultArr);
		}
		
		public function run() {
			if(empty($this->text)) {
				return;
			}
			if(!empty($this->selectedCheckboxes['checkbox4'])) {
				// 28 лютага 2020 года
				$pattern = "/[^\d](([123]?\d) (студзеня|лютага|сакавіка|красавіка|мая|траўня|чэрвеня|ліпеня|жніўня|верасня|кастрычніка|лістапада|снежня) (20[012]\d))( года)?/";
				preg_match_all($pattern, $this->text, $matches, PREG_SET_ORDER);
				foreach($matches as $match) {
					$numberInTextForm = $this->date2text($match[2], $match[3], $match[4]);
					if($numberInTextForm !== false) {
						$decodedFragment = str_replace($match[1], $numberInTextForm . " года", $match[0]);
						$this->text = str_replace($match[0], $decodedFragment, $this->text);
					}
				}
			}
			if(!empty($this->selectedCheckboxes['checkbox3'])) {
				$pattern = "/[^\d](([\d]{1,2})\-(я|й))/";
				preg_match_all($pattern, $this->text, $matches, PREG_SET_ORDER);
				foreach($matches as $match) {
					$numberInTextForm = $this->number2case($match[2], $match[3]);
					if($numberInTextForm !== false) {
						$decodedFragment = str_replace($match[1], $numberInTextForm, $match[0]);
						$this->text = str_replace($match[0], $decodedFragment, $this->text);
					}
				}
			}
			if(!empty($this->selectedCheckboxes['checkbox2'])) {
				$pattern = "/[^\d]((-?[\d]{1,18}) ?(%))/";
				preg_match_all($pattern, $this->text, $matches, PREG_SET_ORDER);
				foreach($matches as $match) {
					$numberInTextForm = $this->number2string($match[2], "percent");
					$decodedFragment = str_replace($match[1], $numberInTextForm, $match[0]);
					$this->text = str_replace($match[0], $decodedFragment, $this->text);
				}
			}
			if(!empty($this->selectedCheckboxes['checkbox1'])) {
				$pattern = "/[^\d](-?[\d]{1,18})[^\d]/";
				preg_match_all($pattern, $this->text, $matches, PREG_SET_ORDER);
				foreach($matches as $match) {
					$numberInTextForm = $this->number2string($match[1]);
					$decodedFragment = str_replace($match[1], $numberInTextForm, $match[0]);
					$this->text = str_replace($match[0], $decodedFragment, $this->text);
				}
			}
			$this->result = $this->text;
		}
		
		public function saveCacheFiles() {
			mb_internal_encoding('UTF-8');
			mb_regex_encoding('UTF-8');
			
			$dateCode = date('Y-m-d_H-i-s', time());
			$randCode = rand(0, 1000);
			$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
			
			$root = $_SERVER['HTTP_HOST'];
			if($root == 'corpus.by') $root = 'https://corpus.by';
			$serviceName = 'NumericExpressionsProcessor';
			$sendersName = 'Numeric Expressions Processor';
			$recipient = 'corpus.by@gmail.com';
			$subject = "$sendersName from IP $ip";
			$mailBody = 'Вітаю, гэта corpus.by!' . self::BR;
			$mailBody .= "$root/$serviceName/ дасылае інфармацыю аб актыўнасці карыстальніка з IP $ip." . self::BR . self::BR;
			
			$cachePath = dirname(dirname(__FILE__)) . "/_cache";
			if(!file_exists($cachePath)) mkdir($cachePath);
			$cachePath = "$cachePath/$serviceName";
			if(!file_exists($cachePath)) mkdir($cachePath);
			
			$filename = $dateCode . '_'. $ip . '_' . $randCode . '_in.txt';
			$path = "$cachePath/in/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			$cacheText = preg_replace("/(^\s+)|(\s+$)/us", '', $this->text);
			fwrite($newFile, $cacheText);
			fclose($newFile);
			$url = $root . "/showCache.php?s=NumericExpressionsProcessor&t=in&f=$filename";
			if(mb_strlen($cacheText)) {
				$mailBody .= "Уваходны тэкст пачынаецца з:" . self::BR;
				preg_match('/([^\n]*\n?){1,3}/u', $cacheText, $matches);
				$mailBody .= '<blockquote><i>' . str_replace("\n", self::BR, trim($matches[0])) . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
			}
			$mailBody .= self::BR;
			
			$filename = $dateCode . '_'. $ip . '_' . $randCode . '_table_out.txt';
			$path = "$cachePath/out/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			$cacheText = preg_replace("/(^\s+)|(\s+$)/us", '', $this->text);
			fwrite($newFile, $cacheText);
			fclose($newFile);
			$url = $root . "/showCache.php?s=$serviceName&t=in&f=$filename";
			if(mb_strlen($cacheText)) {
				$mailBody .= "Тэкст ($textLength сімв., прыкладна $pages ст. па 2300 сімв. на старонку) пачынаецца з:" . self::BR;
				preg_match('/([^\n]*\n?){1,3}/u', $cacheText, $matches);
				$str = str_replace("\n", self::BR, trim($matches[0]));
				if(mb_strlen($str) < 300) {
					$mailBody .= '<blockquote><i>' . $str . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
				}
				else {
					$mailBody .= '<blockquote><i>' . mb_substr($str, 0, 300) . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
				}
			}
			$mailBody .= self::BR;
			
			$filename = $dateCode . '_'. $ip . '_' . $randCode . '_out.txt';
			$path = "$cachePath/out/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			$cacheText = preg_replace("/(^\s+)|(\s+$)/us", "", $this->result);
			fwrite($newFile, $cacheText);
			fclose($newFile);
			$url = $root . "/showCache.php?s=$serviceName&t=out&f=$filename";
			if(mb_strlen($cacheText)) {
				$mailBody .= "Вынік пачынаецца з:" . self::BR;
				preg_match('/([^\n]*\n?){1,3}/u', $cacheText, $matches);
				$mailBody .= '<blockquote><i>' . str_replace("\n", self::BR, trim($matches[0])) . " <a href=$url>паглядзець цалкам</a></i></blockquote>";
			}
			$mailBody .= self::BR;
			
			$header  = "MIME-Version: 1.0\r\n";
			$header .= "Content-type: text/html; charset=utf-8\r\n";
			$header .= "From: $sendersName <corpus.by@gmail.com>\r\n";
			mail($recipient, $subject, $mailBody, $header);
			
			$filename = $dateCode . '_' . $ip . '_' . $randCode . '_e.txt';
			$path = "$cachePath/email/";
			if(!file_exists($path)) mkdir($path);
			$filepath = $path . $filename;
			$newFile = fopen($filepath, 'wb') OR die('open cache file error');
			fwrite($newFile, join("\n", array("recipient: \t" . $recipient, "subject: \t" . $subject, "\n\tMAIL BODY\n\n" . $mailBody, $header)));
			fclose($newFile);
		}
		
		public function getResult() {
			return $this->result;
		}
	}
?>